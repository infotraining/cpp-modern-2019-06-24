#include <algorithm>
#include <iostream>
#include <map>
#include <numeric>
#include <set>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

int foo(int)
{
    return 42;
}

TEST_CASE("decltype")
{
    map<int, string> dict = {{1, "one"}, {2, "two"}, {3, "three"}};

    decltype(dict) dict2;
    REQUIRE(dict2.size() == 0);

    decltype(dict2[0]) word = dict[1]; // no-side effect - non-evaluated context

    static_assert(is_same_v<decltype(word), string&>);
    static_assert(is_same_v<decltype(foo), int(int)>);

    [[maybe_unused]] decltype(foo(declval<int>())) result = foo(123); // declval<T>() - substitite for an object of T

    SECTION("using decltype with lambda expressions")
    {
        auto ptr_comp = [](auto a, auto b) { return *a < *b; };
        set<int*, decltype(ptr_comp)> items{ptr_comp};

        int x = 24;
        int y{7};
        int z{0};

        items.insert(&x);
        items.insert(&y);
        items.insert(&z);

        for (const auto& item : items)
        {
            cout << *item << " ";
        }
        cout << "\n";
    }
}

template <typename T>
auto multiply(const T& a, const T& b) //-> decltype(a * b)
{
    return a * b;
}

//auto multiply_in_cpp20(auto a, auto b)
//{
//    return a * b;
//}
