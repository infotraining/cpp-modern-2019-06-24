#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct X
{
    int  value;

    X(int v, const string& name) :
        value{v}
    {
        cout << "X(" << value << ", " << name << ")\n";
    }

    X()
    {
        static int gen_id = 0;
        value = ++gen_id;

        cout << "X(" << value << ")\n";
    }

    X(const X&) = delete;
    X(X&&) = delete;
    X& operator=(const X&) = delete;
    X& operator=(X&&) = delete;

    ~X()
    {
        cout << "~X(" << value << ")\n";
    }
};

namespace Legacy
{
    X* get_x();

    X* get_x()
    {
        return new X(665, "less evil");
    }

    void use_and_destroy(X* ptr)
    {
        if (ptr)
            cout << "using X: " << ptr->value << endl;

        delete ptr;
    }

    void use(X* ptr)
    {
        if (ptr)
            cout << "using X: " << ptr->value << endl;
    }
}

TEST_CASE("legacy hell")
{
    using namespace Legacy;

    X x{13, "abc"};

    //use(&x);

    use_and_destroy(get_x());
}

unique_ptr<X> get_x()
{
    return make_unique<X>(665, "harmless");
}

void use(X* ptr)
{
    if (ptr)
        cout << "using X: " << ptr->value << endl;
}

void use(X& x)
{
    cout << "using X: " << x.value << endl;
}

void use(unique_ptr<X> ptr) // transfering ownership of ptr
{
    if (ptr)
        cout << "using X: " << ptr->value << endl;
}

TEST_CASE("smart pointers")
{
    unique_ptr<X> ptr = get_x();

    cout << "X: " << ptr->value << endl;

    Legacy::use(ptr.get());

    Legacy::use_and_destroy(ptr.release());

    REQUIRE(ptr == nullptr);

    auto ptr_x = make_unique<X>(87, "eighty seven");

    use(std::move(ptr_x));
}

TEST_CASE("storing unique_pointers in vector")
{
    cout << "\n===============\n";

    vector<unique_ptr<X>> vec_ptrs;

    X* raw_ptr = new X{13, "th"};
    unique_ptr<X> uptr{raw_ptr};

    vec_ptrs.push_back(get_x());
    vec_ptrs.push_back(std::move(uptr));
    vec_ptrs.push_back(make_unique<X>(899, "899"));

    for(const auto& ptr : vec_ptrs)
        cout << ptr->value << " ";
    cout << "\n";

    use(*vec_ptrs[0]);
}

//////////////////////////////////////////////
// storing unique_ptr as a class member

struct WidgetHandler
{
    X x;
};

class Widget
{
    std::unique_ptr<WidgetHandler> ptr_;
public:
    Widget(std::unique_ptr<WidgetHandler>&& p) : ptr_{std::move(p)}
    {
    }

    void use()
    {
        cout << "using : " << ptr_->x.value;
    }

    WidgetHandler& borrow() const
    {
        return *ptr_;
    }

    std::unique_ptr<WidgetHandler> release()
    {
        return std::move(ptr_);
    }
};

TEST_CASE("using widget")
{
    Widget wdt{make_unique<WidgetHandler>()};

    wdt.use();

    WidgetHandler& borrowed = wdt.borrow();
    cout << "borrowed: " << borrowed.x.value << endl;

    unique_ptr<WidgetHandler> hndl = wdt.release();
}

////////////////////////////////////
// custom deallocators

using FileHandler = unique_ptr<FILE, int(*)(FILE*)>;

struct Socket
{
    void send()
    {}

    void cleanup()
    {
        cout << "socket cleanup\n";
    }

    void close()
    {
        cout << "socket closed\n";
    }
};

auto socket_closer = [](Socket* s) { s->cleanup(); s->close(); };

using SocketHandler = unique_ptr<Socket, decltype(socket_closer)>;

void log(FileHandler f)
{
    fprintf(f.get(), "Log: %d", 42);
}

TEST_CASE("custom dellocator")
{
    SECTION("legacy")
    {
        FILE* f = fopen("data.txt", "w");

        fprintf(f, "%d", 42);
        //...

        if (f)
            fclose(f);
    }

    SECTION("unique_ptr")
    {
        FileHandler f{fopen("data.txt", "w"), &fclose};

        auto& deleter = f.get_deleter();

        if (!f)
            exit(13);

        fprintf(f.get(), "%d", 42);

        log(std::move(f));

        {
            Socket s;
            SocketHandler sh{&s, socket_closer};

            s.send();
        }
    }
}

namespace Legacy
{
    int* create_buffer()
    {
        return new int[1024];
    }
}


TEST_CASE("unique_ptr - legacy arrays")
{
    unique_ptr<int[]> tab{Legacy::create_buffer()};

    tab[0] = 1;
}
