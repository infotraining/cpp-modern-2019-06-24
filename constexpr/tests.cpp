#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

int get_value()
{
    return 42;
}

constexpr size_t factorial(size_t n)
{
    return (n == 0) ? 1 : n * factorial(n - 1);
}

TEST_CASE("constexpr")
{
    constexpr int cx1 = 7;
    const int cx2 = get_value();

    constexpr size_t value = factorial(3);
    static_assert(value == 6);

    std::array<int, factorial(4)> tab = {};
    static_assert(tab.size() == 24);
}

template <size_t N>
constexpr std::array<size_t, N> create_factorial_lookup()
{
    std::array<size_t, N> result{};

    for(size_t i = 0; i < N; ++i)
        result[i] = factorial(i);

    return result;
}

template <typename Iterator, typename Pred>
constexpr auto constexpr_find_if(Iterator first, Iterator last, Pred pred)
{
    auto result = last;

    for(auto it = first; it != last; ++it)
    {
        if (pred(*it))
        {
            result = it;
            break;
        }
    }

    if (result != last)
        return *result;
    else
        throw "not found"s;
}

TEST_CASE("constexpr lookup")
{
    constexpr auto factorial_lookup = create_factorial_lookup<5>();

    static_assert(factorial_lookup[0] == 1);
    static_assert(factorial_lookup[1] == 1);
    static_assert(factorial_lookup[2] == 2);
    static_assert(factorial_lookup[3] == 6);
    static_assert(factorial_lookup[4] == 24);

    constexpr auto first_gt_5 = constexpr_find_if(begin(factorial_lookup), end(factorial_lookup), [](int x) { return x > 5; });
    static_assert (first_gt_5 == 6);
}
