#include <cassert>
#include <cstdlib>
#include <iostream>
#include <set>
#include <stdexcept>
#include <string>
#include <memory>

class Observer
{
public:
    virtual void update(const std::string& event_args) = 0;
    virtual ~Observer() {}
};

using WeakObserver = std::weak_ptr<Observer>;

class Subject
{
    int state_;
    std::set<WeakObserver, std::owner_less<WeakObserver>> observers_;

public:
    Subject()
        : state_(0)
    {
    }

    void register_observer(WeakObserver observer)
    {
        observers_.insert(std::move(observer));
    }

    void unregister_observer(WeakObserver observer)
    {
        observers_.erase(std::move(observer));
    }

    void set_state(int new_state)
    {
        if (state_ != new_state)
        {
            state_ = new_state;
            notify("State has been set to: " + std::to_string(state_));
        }
    }

protected:
    void notify(const std::string& event_args)
    {
        auto it = begin(observers_);
        while(it != end(observers_))
        {
            if (auto observer = it->lock())
            {
                observer->update(event_args);
                ++it;
            }
            else
            {
                it = observers_.erase(it);
            }
        }
    }
};

class Customer : public Observer
{
public:
    virtual void update(const std::string& event)
    {
        std::cout << "Customer notified: " << event << std::endl;
    }
};

class Logger : public Observer
{
    std::string path_;

public:
    Logger(const std::string& p)
        : path_{p}
    {
    }

    virtual void update(const std::string& event)
    {
        std::cout << "Logging to " << path_  << ": " << event << std::endl;
    }
};

int main(int argc, char const* argv[])
{
    using namespace std;

    Subject s;

    {
        auto o1 = make_shared<Customer>();
        s.register_observer(o1);

        {
            auto logger = make_shared<Logger>("log.dat");
            s.register_observer(logger);

            s.set_state(1);
            s.set_state(2);
            s.set_state(3);            

            cout << "End of scope.\n\n" << endl;
        }

        s.set_state(42);
        s.set_state(665);
        s.unregister_observer(o1);
    }
}
