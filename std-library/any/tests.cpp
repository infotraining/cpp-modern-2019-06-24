#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <any>

#include "catch.hpp"

using namespace std;

TEST_CASE("any")
{
	any ao;

	REQUIRE(ao.has_value() == false);

	ao = 1;
	ao = 3.14;
	ao = "text"sv;
	ao = "abc"s;
	ao = vector{ 1, 2, 3 };

	SECTION("any_cast")
	{
		auto data = any_cast<vector<int>>(ao);
		REQUIRE(data == vector{ 1, 2, 3 });

		REQUIRE_THROWS_AS(any_cast<string>(ao), bad_any_cast);
	}

	SECTION("any_cast with pointer")
	{
		ao = 2;

		if (auto* ptr = any_cast<vector<int>>(&ao); ptr)
		{
			for (const auto& item : *ptr)
				cout << item << " ";
			cout << '\n';
		}
		else
		{
			cout << "Bad access to any" << endl;
		}
	}

	SECTION("type")
	{
		auto& type_desc = ao.type();
		cout << type_desc.name() << endl;
	}

	SECTION("emplace")
	{
		ao.emplace<string>("text");

		auto text = any_cast<const string>(ao);

		cout << text << endl;
	}
}