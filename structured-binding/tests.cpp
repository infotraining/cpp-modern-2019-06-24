#include <algorithm>
#include <iostream>
#include <map>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

auto get_coord() -> int (&)[3]
{
    static int coord[3] = {1, 2, 3};

    return coord;
}

auto get_stat()
{
    return tuple{1, 3.14, "pi"s};
}

struct Error
{
    int c;
    const char* desc;
};

[[nodiscard]] Error get_error()
{
    return {13, "Error"};
}

TEST_CASE("structured binding")
{
    SECTION("c-style arrays")
    {
        auto [x, y, z] = get_coord();

        cout << x << " " << y << " " << z << "\n";
    }

    SECTION("tuple")
    {
        auto [i, pi, text] = get_stat();

        cout << text << "\n";
    }

    SECTION("struct")
    {
        auto [error_code, error_msg] = get_error();

        cout << error_code << " - " << error_msg << endl;
    }

    SECTION("pairs")
    {
        map<int, string> dict = {{1, "one"}};

        if (auto [pos_it, was_inserted] = dict.insert(make_pair(2, "two")); was_inserted)
        {
            auto [key, value] = *pos_it;
            cout << "value was inserted: " << value << endl;
        }

        [[maybe_unused]] int x = 42;
    }
}

int get_index()
{
    return 42;
}

TEST_CASE("if with init section")
{
    for (int i = 0; i < 10; ++i)
    {
    }

    if (int i = get_index(); i < 100)
    {
    }

    Error err = get_error();

    vector<int> data;
}
