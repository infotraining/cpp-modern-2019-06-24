#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>

#include "catch.hpp"

using namespace std;

TEST_CASE("auto")
{
    auto i = 42; // int

    auto ptr1 = &i;
    auto* ptr2 = &i;

    auto d = 3.14; // double

    auto f = 3.14f; // float

    auto result = i * f; // float

    SECTION("iteration over container")
    {
        int vec[] = {1, 2, 3, 4};
        auto it = begin(vec); // int*

        for(auto cit = cbegin(vec); it != cend(vec); ++it)
        {
            cout << *cit << " ";
        }
        cout << "\n";
    }
}

///////////////////////////////////
// type deduction using auto
//

template <typename Arg>
void deduce1(Arg arg)
{
    puts(__PRETTY_FUNCTION__);
}

void foo(int)
{}

TEST_CASE("auto - case1")
{
    int x = 10;
    const int cx = 42;
    int& ref_x = x;
    const int& cref_x = x;

    deduce1(x);
    auto ax = x;

    deduce1(cx);
    auto ax2 = cx; // const is removed - int deduced

    deduce1(ref_x);
    auto a3 = ref_x; // ref is removed - int deduced

    deduce1(cref_x);
    auto a4 = cref_x; // const & ref are removed - int deduced

    int tab[3] = {1, 2, 3};
    deduce1(tab);
    auto atab = tab; // decay to pointer - int*

    deduce1(foo);
    auto af = foo; // void(*)(int)
}

template <typename Arg>
void deduce2(Arg& arg)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("auto& - case 2")
{
    int x = 10;
    const int cx = 42;
    int& ref_x = x;
    const int& cref_x = x;

    deduce2(x);
    auto& ax1 = x; // int&

    deduce2(cx);
    auto& ax2 = cx; // const int&

    deduce2(ref_x);
    auto& ax3 = ref_x; // int&

    deduce2(cref_x);
    auto& ax4 = cref_x; // const int&

    int tab[3] = {1, 2, 3};
    deduce2(tab);
    auto& atab = tab; // int(&)[3] - ref to array of ints

    deduce2(foo);
    auto& af = foo; // void(&)(int)
}


template <typename Arg>
void deduce3(Arg&& arg)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("auto - case 3")
{
    deduce3(3.14);
    auto&& dx = 3.14;

    int x = 10;
    deduce3(x);
    auto&& ax = x;
}

TEST_CASE("auto - direct init")
{
    auto ax = 1;  // int
    auto ay(1); // int

    SECTION("since C++17")
    {
        auto az{1}; // int

        auto il = {1}; // std::initializer_list<int>
    }
}


auto bar(int x)
{
    if (x == 42)
        return "The Anwser"s;
    return "I Don't know"s;
}

decltype(auto) get_value(map<int, string>& dict, int key)
{
    return dict.at(key);
}

decltype(auto) bad_code()
{
    int x = 42;

    return (x); // now - it is int&
}

TEST_CASE("auto type deduction for function")
{
    map<int, string> dict = { { 1, "one"}, {2 , "two" } };

    get_value(dict, 1) = "jeden";

    for(const auto& [key, value] : dict)
    {
        cout << key << " - " << value << endl;
    }
}

TEST_CASE("auto&& - use case")
{
    vector<bool> bits = {1, 0, 0, 1};

    for(auto&& bit : bits)
        bit.flip();


    vector<string> words;

    auto&& obj = bar();

    //..

    obj[0] = "P";

    //..
    words.push_back(std::move(obj));


    auto pusher = [&words](auto&& item) { words.push_back(std::forward<decltype(item)>(item)); };

    pusher("text"s);
}
