#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct IGadget
{
    virtual ~IGadget() = default;
    virtual void info() const = 0;
};

struct Gadget : IGadget
{
private:
    inline static int id_{}; // since c++17

    static int get_id()
    {
        return ++id_;
    }

public:
    int id = get_id();
    string name = "unknown";

    Gadget() = default;

    Gadget(string name)
        : Gadget{get_id(), std::move(name)} // delegation to other constructor
    {
        // extra impl
    }    

    Gadget(int id, string name)
        : id{id}, name{std::move(name)}
    {
    }

    void info() const override
    {
        cout << "Gadget(id: " << id << ", " << name << ")\n";
    }

    ~Gadget()
    {
        cout << "destructor of gadget\n";
    }
};

class SuperGadget : public Gadget
{
public:
    using Gadget::Gadget; // inheritance of constructors

    SuperGadget(int id) : SuperGadget{id, "gadget-"s + to_string(id) }
    {
        //throw 42;
    }

    ~SuperGadget()
    {
        cout << "destructor of super gadget\n";
    }

    void info() const override
    {
        cout << "SuperGadget(id: " << id << ", " << name << ")\n";
    }
};

class NoCopyable
{
public:
    NoCopyable() = default;
    NoCopyable(const NoCopyable&) = delete;
    NoCopyable& operator=(const NoCopyable&) = delete;
};

template <typename T>
enable_if_t<is_same_v<T, int>> calculate(T x)
{
    cout << "calculate(x: " << x << ")\n";
}

void calculate(double) = delete;

TEST_CASE("NoCopyable")
{
    NoCopyable nc;
    // NoCopyable backup = nc; // ERROR

    short sx = 14;
    //calculate(sx); // ERROR

    calculate(42);
    //calculate(3.14f); // ERROR
}

TEST_CASE("classes")
{
    Gadget g1{1, "ipad"};
    Gadget g2;

    cout << g2.id << " - " << g2.name << "\n";

    SuperGadget sg1{13, "ipod"};
    SuperGadget sg2{"mp3 player"};
}

TEST_CASE("super gadge destruction")
{
    cout << "\n===================\n";
    try
    {
        SuperGadget sg3{665};
        Gadget& ref_g = sg3;
        ref_g.info();
    }
    catch(...)
    {}
}
