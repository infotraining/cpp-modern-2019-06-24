#include <cassert>
#include <chrono>
#include <iostream>
#include <stdexcept>
#include <thread>
#include <vector>

#include "raii_threads.hpp"

using namespace std;

namespace Raii
{
    namespace WithRef
    {
        class JoiningThread
        {
            thread& thd_;

        public:
            JoiningThread(thread& thd)
                : thd_{thd}
            {
            }

            JoiningThread(const JoiningThread&) = delete;
            JoiningThread& operator=(const JoiningThread&) = delete;

            thread& get()
            {
                return thd_;
            }

            ~JoiningThread()
            {
                if (thd_.joinable())
                    thd_.join();
            }
        };

        class DetachedThread
        {
            thread& thd_;

        public:
            DetachedThread(thread& thd)
                : thd_{thd}
            {
            }

            DetachedThread(const DetachedThread&) = delete;
            DetachedThread& operator=(const DetachedThread&) = delete;

            thread& get()
            {
                return thd_;
            }

            ~DetachedThread()
            {
                if (thd_.joinable())
                    thd_.detach();
            }
        };
    }

    namespace WithValue
    {
        class JoiningThread
        {
            thread thd_;

        public:
            JoiningThread(thread&& thd)
                : thd_{std::move(thd)}
            {
            }

            JoiningThread(const JoiningThread&) = delete;
            JoiningThread& operator=(const JoiningThread&) = delete;
            JoiningThread(JoiningThread&&) = default;
            JoiningThread& operator=(JoiningThread&&) = default;

            thread& get()
            {
                return thd_;
            }

            ~JoiningThread()
            {
                if (thd_.joinable())
                    thd_.join();
            }
        };
    }
}

void background_work(int id, chrono::milliseconds delay)
{
    for (int i = 0; i < 10; ++i)
    {
        std::cout << "THD#" << id << ": " << i << std::endl;

        this_thread::sleep_for(delay);
    }
}

void may_throw()
{
    throw runtime_error("Error");
}

void local_scope1()
{
    using namespace Raii::WithRef;

    // 1-szy sposób
    thread thd1(&background_work, 1, 100ms);
    JoiningThread raii_thd{thd1};

    thread thd2(&background_work, 2, 200ms);
    DetachedThread detached_thread{thd2};

    may_throw();
}

void local_scope2()
{
    using namespace Raii;

    {
        std::thread thd{[] { cout << "hello from thread;\n"; } };
        WithValue::JoiningThread j_thd{std::move(thd)};
    }


    JoiningThread raii_thd1{in_place, &background_work, 3, 100ms};
    JoiningThread raii_thd2{in_place, thread{&background_work, 4, 100ms}};

    //JoiningThread illegal_copy = raii_thd1;
    JoiningThread moved_raii_thd = move(raii_thd1);
    assert(raii_thd1.get().get_id() == thread::id{});

    vector<JoiningThread> threads;
    threads.push_back(move(moved_raii_thd));
    threads.push_back(move(raii_thd2));
    threads.emplace_back(in_place, &background_work, 5, 400ms);

    may_throw();
}

int main()
{
    try
    {
        local_scope1();
    }
    catch (const runtime_error& e)
    {
        cout << e.what() << endl;
    }

    cout << "\n\n---------------------\n\n";

    try
    {
        local_scope2();
    }
    catch (const runtime_error& e)
    {
        cout << e.what() << endl;
    }
}
