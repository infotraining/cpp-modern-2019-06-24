#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <type_traits>
#include <cstddef>

#include "catch.hpp"

using namespace std;

enum Coffee : uint8_t; // forward declaration of enum

void foo(Coffee c);

// defintion of type
enum Coffee : uint8_t { Espresso, Cappucino, Latte };

TEST_CASE("enums")
{
    Coffee c = Espresso;

    uint8_t index = c; // possible bug
    std::underlying_type_t<Coffee> safe_index = c;

    REQUIRE(index == 0);

    c = static_cast<Coffee>(2);
    REQUIRE(c == Latte);

    cout << "kawa: " << c << "\n";
}

enum class DayOfWeek : uint8_t { Mon = 1, Tue, Wed, Thd, Fri, Sat, Sun };

TEST_CASE("scoped enums")
{
    DayOfWeek d = DayOfWeek::Mon;

    auto index = static_cast<int>(d);
    REQUIRE(index == 1);

    d = static_cast<DayOfWeek>(7);
    REQUIRE(d == DayOfWeek::Sun);

    cout << "dzień: " << static_cast<int>(d) << "\n";

    //auto [value, desc] = d;
}

TEST_CASE("using enum classes")
{
    DayOfWeek d{255};

    std::byte bt1{42};

    bt1 <<= 2;

    cout << to_integer<int>(bt1) << endl;
}

/////////////////////////////
// bit flags

template<typename E>
struct EnableBitmaskOperators : std::false_type
{
};

template<typename E>
std::enable_if_t<EnableBitmaskOperators<E>::value, E>
operator |(E lhs, E rhs)
{
    typedef typename std::underlying_type<E>::type underlying;
    return static_cast<E>(
        static_cast<underlying>(lhs) | static_cast<underlying>(rhs));
}

enum class MyBitmask
{
    first = 1, second = 2, third = 4
};

template<>
struct EnableBitmaskOperators<MyBitmask> : std::true_type
{
};


