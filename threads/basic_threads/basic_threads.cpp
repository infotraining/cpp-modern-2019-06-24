#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

using namespace std::literals;

void hello()
{
    std::string text = "Hello Concurrent World";

    for (const auto& c : text)
    {
        std::cout << c << " ";
        std::this_thread::sleep_for(100ms);
        std::cout.flush();
    }

    std::cout << std::endl;
}

void background_work(size_t id, const std::string& text, std::chrono::milliseconds delay)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    for (const auto& c : text)
    {
        std::cout << "bw#" << id << ": " << c << std::endl;

        std::this_thread::sleep_for(delay);
    }

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

class BackgroundWork
{
    const size_t id_;

public:
    BackgroundWork(size_t id)
        : id_{id}
    {
    }

    void operator()(const std::string& text, std::chrono::milliseconds delay)
    {
        std::cout << "BW#" << id_ << " has started..." << std::endl;

        for (const auto& c : text)
        {
            std::cout << "BW#" << id_ << ": " << c << std::endl;

            std::this_thread::sleep_for(delay);
        }

        std::cout << "BW#" << id_ << " is finished..." << std::endl;
    }
};

template <typename Container>
void print(const Container& c, const std::string& prefix)
{
    std::cout << prefix << ": [ ";
    for (const auto& item : c)
    {
        std::cout << item << " ";
    }

    std::cout << "]" << std::endl;
}

void run_as_deamon()
{
    std::thread thd(BackgroundWork(99), "deamon", 2000ms);

    thd.detach();

    if (!thd.joinable())
        std::cout << "Thread has been detached..." << std::endl;
}

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    run_as_deamon();

    std::thread thd1(&hello);
    std::thread thd2(&hello);

    hello();

    thd1.join();
    thd2.join();

    thd1 = std::thread([] { std::cout << "new task..." << std::endl; std::this_thread::sleep_for(5s);
        std::cout << "end of task..." << std::endl;
    });

    thd1.join();

    std::cout << "\n-------------\n\n";

    std::thread thd3(&background_work, 1, "multithreading", 100ms);
    std::thread thd4(&background_work, 2, "threads", 200ms);

    BackgroundWork bw1(1);
    std::thread thd5(bw1, "concurrent", 200ms);
    std::thread thd6(BackgroundWork(2), "parallel", 300ms);

    thd3.join();
    thd4.join();
    thd5.join();
    thd6.join();

    std::cout << "\n-------------\n\n";

    const std::vector<int> data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::vector<int> target1;
    std::vector<int> target2(data.size());

    std::thread thd7([&data, &target1] { target1.assign(data.begin(), data.end()); });
    std::thread thd8([&data, &target2] { std::copy(data.begin(), data.end(), target2.begin()); });

    std::thread thd9 = std::move(thd8);

    thd8 = std::move(thd7);

    thd8.join();
    thd9.join();

    print(target1, "target1");
    print(target2, "target2");

    std::cout << "Main thread ends..." << std::endl;
}
