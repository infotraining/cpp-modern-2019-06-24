#include <atomic>
#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>

using namespace std;

const int max_iterations = 100000000;
int counter = 0;

void worker()
{
    for (int i = 0; i < max_iterations; ++i)
    {
        counter++;
    }
}

int main()
{
    auto start = chrono::high_resolution_clock::now();

    thread th1(worker);
    thread th2(worker);
    th1.join();
    th2.join();

    auto end = chrono::high_resolution_clock::now();
    float mseconds = chrono::duration_cast<chrono::milliseconds>(end - start).count();
    cout << "Time: " << mseconds << " msec" << endl;

    cout << "Counter = " << counter << endl;
}
